Setting up a hard or soft real-time system
==========================================

This guide explains how to setup a hard and soft real-time (RT) systems on Linux,  
specifically for Ubuntu 14.04 LTS.  
For an explanation on hard vs soft real-time (rt) sytem, please see [UbuntuStudio/RealTimeKernel].  
For most systems, the following should be sufficient:  

*Linux low latency (soft RT), with a high priority or real-time thread*

There are several benefits of soft over hard real-time.  
Particularly, item 1 can be a troublesome to fix.  

1. Graphics drivers work as normal  
2. Doesn't sacrifice power saving features  
3. Easy to install  

Low latency kernal (soft RT)
----------------------------
You may want to install the same version as your current kernel ($ uname -a).  
This assumes are using Ubuntu 14.04.01 LTS, with Kernel 3.13.0  
You will need to download the following three files, then install.  

* all   : linux-headers-3.13.0-45_3.13.0-45.74_all.deb  
* header: linux-headers-lowlatency_3.13.0.45.52_amd64.deb  
* image : linux-image-lowlatency_3.13.0.45.52_amd64.deb  

```sh
$ cd ~
$ apt-get download linux-headers-3.13.0-45
$ apt-get download linux-headers-3.13.0-45-lowlatency 
$ apt-get download linux-image-3.13.0-45-lowlatency
$ sudo dpkg -i *.deb
$ sudo update-grub
```

RT Preempt kernal (hard RT)
---------------------------


Install a [mainstream kernel] version that matches an [RT kernel] version.  
Instructions based upon the [rt kernel wiki article] and the [kernelnewbies article].  
The kernel version should be higher than the default Ubuntu Kernel ($ uname -a).  
The following assumes you want kernel 3.14.34
```sh
$ cd ~
$ wget ftp://ftp.kernel.org/pub/linux/kernel/v3.x/linux-3.14.34.tar.gz
$ tar -xvzf linux-3.14.34.tar.gz
```
download the corresponding realtime patch to ~ folder:  
https://www.kernel.org/pub/linux/kernel/projects/rt/3.14/patch-3.14.34-rt31.patch.xz
```sh
$ cd linux-3.14.34
$ xzcat ../patch-3.14.34-rt31.patch.xz | patch -p1
```
Copy the config from the closest generic (or low latency) version kernel config file
```sh
$ cp /boot/config-3.13.0-36-generic .config
```
Open up the config, and make the following changes  
***Kernel 2.6.x:***  
CONFIG_PREEMPT=y  
CONFIG_PREEMPT_RT=y  
***Kernel 3.x:***  
CONFIG_PREEMPT=y  
CONFIG_PREEMPT_RT_BASE=y  
CONFIG_PREEMPT_RT_FULL=y  
CONFIG_HIGH_RES_TIMERS=y  
```sh
$ make -j3
```
During the installation prompts, pick fully preemptible rt.  
Use enter to leave all else at default.  
```sh
$ sudo make modules_install install
$ sudo update-grub
```
Do not delete the built kernel. If you really want to delete it,

1. first copy it to /usr/src/  
2. redo the links in /lib/modules/3.14.34-rt31/  
```sh
$ cd ~
$ sudo cp -avr ./linux-3.14.34/  /usr/src/linux-3.14.34/
$ cd /lib/modules/3.14-blah
$ sudo rm -vf build source
$ ln -s /usr/src/linux-3.14.34/ build
$ ln -s /usr/src/linux-3.14.34/ source
```
Now you can delete your build kernel in /home/

Uninstalling a Kernel
---------------------
If you would like to uninstall a mainline kernel, find the exact name of the kernel packages.  
Before removing, make sure to boot into a different kernel then the one being removed.
```sh
$ dpkg -l | grep "linux\-[a-z]*\-" <OR> $ dpkg --list | grep kernel-image
$ sudo apt-get remove KERNEL_PACKAGES_TO_REMOVE
$ sudo update-grub
```
OR to also delete configuration files (recommended),  
replace sudo apt-get remove with:  
```sh
$ sudo apt-get purge KERNEL_PACKAGES_TO_REMOVE
```

Uninstalling a Custom Kernel
----------------------------
```sh
$ cd /lib/modules/
$  sudo rm -rf KERNEL_PACKAGE_TO_REMOVE
$ cd /boot/
$ sudo rm sudo rm config-KERNEL_PACKAGE_TO_REMOVE
$ sudo rm initrd.img-KERNEL_PACKAGE_TO_REMOVE
$ sudo rm System.map-KERNEL_PACKAGE_TO_REMOVE
$ sudo rm vmlinuz-KERNEL_PACKAGE_TO_REMOVE
```

Testing Kernel Latency
----------------------
[Cyclictest] can be used to test latency. 
```sh
$ sudo apt-get install libnuma-dev
$ git clone git://git.kernel.org/pub/scm/linux/kernel/git/clrkwllms/rt-tests.git 
$ cd rt-tests
$ make all
$ sudo ./cyclictest -t1 -p 80 -n -i 10000 -l 10000
```

[UbuntuStudio/RealTimeKernel]:https://help.ubuntu.com/community/UbuntuStudio/RealTimeKernel
[mainstream kernel]:ftp.kernel.org/pub/linux/kernel/v3.x/  
[RT kernel]:https://www.kernel.org/pub/linux/kernel/projects/rt/  
[rt kernel wiki article]:https://rt.wiki.kernel.org/index.php/RT_PREEMPT_HOWTO  
[kernelnewbies article]:http://kernelnewbies.org/KernelBuild  
[Cyclictest]:https://rt.wiki.kernel.org/index.php/Cyclictest 