**SimplECAT Library**
======================
Authors : Brian Soe, Arya Banait  
Contact : bsoe at stanford.edu, arya at stanford.edu

Developed in the Stanford Robotics Lab, Stanford University.  

About
-----
Simple C++ Library for ethercat control, built upon the [EtherLab EtherCat Master].  
The included classes assume only the inputs/outputs are of interest.  
All other device entries (e.g. status, min, max) are ignored.  
To add these device, please modify the appropriate slave device header file.  

EtherCAT
-------------
EtherCAT is an Ethernet-based fieldbus system.  
It is suitable for both hard and soft real-time systems.

Example Code
-------------------
```c++
simplecat::Master master;
simplecat::Beckhoff_EK1100 bh_ek1100;
simplecat::Beckhoff_EL4134 bh_el4134;

// set the values between ~2.5V and ~10V
bh_el4134.write_data_[0] =  8000;
bh_el4134.write_data_[1] = 16000;
bh_el4134.write_data_[2] = 24000;
bh_el4134.write_data_[3] = 32000;

master.addSlave(0,0,&bh_ek1100);
master.addSlave(0,1,&bh_el4134);

master.setThreadHighPriority();
master.activate();

while(1){
    //placeholder - do some loop timing
    master.update();
}
```

Install
-------
1. Install a real-time system (optional)  
2. Install EtherLab for that kernel  
3. Download SimplECAT and run the install script  

```sh
git clone https://bsoe@bitbucket.org/bsoe/simplecat.git  
sh install.sh
```

Additional Setup Tutorials
----------------------------------
Installing a soft or hard real-time system on Ubuntu 14.04 LTS:  
[SETUP_RT_LINUX.md]  

Installing EtherLab on Ubuntu 14.04 LTS:  
[SETUP_ETHERLAB.md]

Dependencies
------------
[EtherLab EtherCat Master]



[EtherLab EtherCat Master]:http://www.etherlab.org/en/ethercat/
[SETUP_RT_LINUX.md]:https://bitbucket.org/bsoe/simplecat/src/master/SETUP_RT_LINUX.md
[SETUP_ETHERLAB.md]:https://bitbucket.org/bsoe/simplecat/src/master/SETUP_ETHERLAB.md