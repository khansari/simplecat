/****************************************************************************/

#include <simplecat/Master.h>
#include <simplecat/beckhoff.h>

#include <iostream>
#include <signal.h>
#include <time.h>
#include <string.h>
#include <unistd.h>

/****************************************************************************/

// Application parameters
#define FREQUENCY 100
#define PRIORITY 1

// Optional features
#define CONFIGURE_PDOS  1
#define SDO_ACCESS      0

/****************************************************************************/

// Timer
static unsigned int sig_alarms = 0;
static unsigned int user_alarms = 0;

static unsigned long my_counter = 0;

/****************************************************************************/

void signal_handler(int signum) {
    switch (signum) {
        case SIGALRM:
            sig_alarms++;
            break;
    }
}

/****************************************************************************/

int main(int argc, char **argv)
{
    simplecat::Master master;
    simplecat::Beckhoff_EK1100 bh_ek1100;
    simplecat::Beckhoff_EL4134 bh_el4134;

    // stagger the values between 2.5V and 10V approximately.
    bh_el4134.write_data_[0] =  8000;
    bh_el4134.write_data_[1] = 16000;
    bh_el4134.write_data_[2] = 24000;
    bh_el4134.write_data_[3] = 32000;

    master.addSlave(0,0,&bh_ek1100);
    master.addSlave(0,1,&bh_el4134);

    master.setThreadHighPriority();
    master.activate();

    struct sigaction sa;
    struct itimerval tv;

    sa.sa_handler = signal_handler;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = 0;
    if (sigaction(SIGALRM, &sa, 0)) {
        fprintf(stderr, "Failed to install signal handler!\n");
        return -1;
    }

    printf("Starting timer...\n");
    tv.it_interval.tv_sec = 0;
    tv.it_interval.tv_usec = 1000000 / FREQUENCY;
    tv.it_value.tv_sec = 0;
    tv.it_value.tv_usec = 1000;
    if (setitimer(ITIMER_REAL, &tv, NULL)) {
        fprintf(stderr, "Failed to start timer: %s\n", strerror(errno));
        return 1;
    }

    printf("Started.\n");
    while (1) {
        pause();

#if 0
        struct timeval t;
        gettimeofday(&t, NULL);
        printf("%u.%06u\n", t.tv_sec, t.tv_usec);
#endif

        while (sig_alarms != user_alarms)
        {

            if (my_counter%500==0){
                std::cout << "increasing values by 2.5 V" << std::endl;
                for (int i=0; i<4; ++i){
                    bh_el4134.write_data_[i] += 8000;
                    if (bh_el4134.write_data_[i]>32000){
                        bh_el4134.write_data_[i] = 0;
                    }
                }
            }
            ++my_counter;

            master.update();
            user_alarms++;
        }
    }

    return 0;
}

/****************************************************************************/
