/****************************************************************************/

#include "cForceSensor.h"

#include <simplecat/Master.h>
#include <simplecat/beckhoff.h>

#include <signal.h>
#include <time.h>
#include <string.h>
#include <unistd.h>

#include <iostream>
#include <fstream>

/****************************************************************************/

// Output file
#define OUTPUT_FILE "motor_force_curve_out.txt"
#define STEP_PER_SEC 1000

// Application parameters
#define FREQUENCY 100
#define PRIORITY 1

// Optional features
#define CONFIGURE_PDOS  1
#define SDO_ACCESS      0

/****************************************************************************/

// Timer
static unsigned int sig_alarms = 0;
static unsigned int user_alarms = 0;

/****************************************************************************/

void signal_handler(int signum) {
    switch (signum) {
        case SIGALRM:
            sig_alarms++;
            break;
    }
}

/****************************************************************************/

int main(int argc, char **argv)
{  
	// Create the file out
	std::ofstream output_file;
  	output_file.open(OUTPUT_FILE, std::fstream::out);

	// Initialize the force sensor
	double ForceData[3], TorqueData[3];
    cForceSensor g_ForceSensor;
    g_ForceSensor.Set_Calibration_File_Loc(
		"../motor_force_curve/ATIForceSensor/calibrations/FT16213.cal");
    g_ForceSensor.Initialize_Force_Sensor("/dev/comedi0");
    sleep(2);
    g_ForceSensor.Zero_Force_Sensor();

	// beckhoff stack
    simplecat::Master master;
    simplecat::Beckhoff_EK1100 bh_ek1100;
    simplecat::Beckhoff_EL4134 bh_el4134;

    // stagger the values between 2.5V and 10V approximately.
    bh_el4134.write_data_[0] = 0;
    bh_el4134.write_data_[1] = 0;
    bh_el4134.write_data_[2] = 0;
    bh_el4134.write_data_[3] = 0;
	bool values_increasing = true;

    master.addSlave(0,0,&bh_ek1100);
    master.addSlave(0,1,&bh_el4134);

    //master.setThreadHighPriority();
    master.activate();

    struct sigaction sa;
    struct itimerval tv;

    sa.sa_handler = signal_handler;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = 0;
    if (sigaction(SIGALRM, &sa, 0)) {
        fprintf(stderr, "Failed to install signal handler!\n");
        return -1;
    }

    printf("Starting timer...\n");
    tv.it_interval.tv_sec = 0;
    tv.it_interval.tv_usec = 1000000 / FREQUENCY;
    tv.it_value.tv_sec = 0;
    tv.it_value.tv_usec = 1000;
    if (setitimer(ITIMER_REAL, &tv, NULL)) {
        fprintf(stderr, "Failed to start timer: %s\n", strerror(errno));
        return 1;
    }

    struct timeval t_start;
    gettimeofday(&t_start, NULL);

    printf("Started.\n");
    while (1) {
        pause();

        while (sig_alarms != user_alarms)
        {
			// acquire force data
			g_ForceSensor.AcquireFTData();
			g_ForceSensor.GetForceReading(ForceData);
			g_ForceSensor.GetTorqueReading(TorqueData);

			struct timeval t;
			gettimeofday(&t, NULL);

			double dt = (t.tv_sec -t_start.tv_sec)
					  + 0.000001*(t.tv_usec-t_start.tv_usec);

			output_file << dt << '\t'
						<< bh_el4134.write_data_[0] << '\t'
						<< ForceData[2] 			<< '\n';

			if (values_increasing){
				bh_el4134.write_data_[0] += STEP_PER_SEC/FREQUENCY;
				if (bh_el4134.write_data_[0]>16000){
					values_increasing = false;
					bh_el4134.write_data_[0] = 16000;
				}
			} else {
				bh_el4134.write_data_[0] -= STEP_PER_SEC/FREQUENCY;
				if (bh_el4134.write_data_[0]<-16000){
					values_increasing = true;
					bh_el4134.write_data_[0] = -16000;
				}
			}

			//update the beckhoff stack master
            master.update();
            user_alarms++;
        }
    }

  	output_file.close();
    return 0;
}

/****************************************************************************/
