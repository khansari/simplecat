#include "cForceSensor.h"

#include <simplecat/Master.h>
#include <simplecat/beckhoff.h>

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <sched.h>
#include <sys/mman.h>
#include <string.h>

#define MY_PRIORITY 	(49) /* we use 49 as the PRREMPT_RT use 50
                            as the priority of kernel tasklets
                            and interrupt handler by default */

#define MAX_SAFE_STACK 	(8*1024) /* The maximum stack size which is
                                   guaranteed safe to access without
                                   faulting */

#define NSEC_PER_SEC   	(1000000000) /* The number of nsecs per sec. */

static unsigned long my_counter = 0;

void stack_prefault(void)
{
    unsigned char dummy[MAX_SAFE_STACK];
    memset(dummy, 0, MAX_SAFE_STACK);
    return;
}

int main()
{
	/* RT PREEMPT declarations */	
	struct timespec t;
    struct sched_param param;
    int interval = 500000000; /* 1 nanosecond per tick */
	
	/* Program declarations */
	double ForceData[3], TorqueData[3];

    /* Declare ourself as a real time task */
    param.sched_priority = MY_PRIORITY;
    if(sched_setscheduler(0, SCHED_FIFO, &param) == -1)
	{
		perror("sched_setscheduler failed");
        exit(-1);
    }

    /* Lock memory */
    if(mlockall(MCL_CURRENT|MCL_FUTURE) == -1) 
	{
        perror("mlockall failed");
        exit(-2);
    }

    /* Pre-fault our stack */
    stack_prefault();
    clock_gettime(CLOCK_MONOTONIC ,&t);
    
	/* Initialize the force sensor */
    cForceSensor g_ForceSensor;
    g_ForceSensor.Set_Calibration_File_Loc("../motor_force_curve/ATIForceSensor/calibrations/FT16213.cal");
    g_ForceSensor.Initialize_Force_Sensor("/dev/comedi0");
    sleep(2);
    g_ForceSensor.Zero_Force_Sensor();

	/** Initialize the Bechhoff stack */
	// beckhoff stack
    simplecat::Master master;
    simplecat::Beckhoff_EK1100 bh_ek1100;
    simplecat::Beckhoff_EL4134 bh_el4134;

    // stagger the values between 2.5V and 10V approximately.
    bh_el4134.write_data_[0] =  8000;
    bh_el4134.write_data_[1] = 16000;
    bh_el4134.write_data_[2] = 24000;
    bh_el4134.write_data_[3] = 32000;

    master.addSlave(0,0,&bh_ek1100);
    master.addSlave(0,1,&bh_el4134);

    master.activate();
	
	/* start after one second */
	t.tv_sec++;    

    while(1)
    {
		/* wait until next shot */
        clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &t, NULL);

		/* stuffs to run in real-time loop */

		//update the force sensor
        g_ForceSensor.AcquireFTData();
        g_ForceSensor.GetForceReading(ForceData);
        g_ForceSensor.GetTorqueReading(TorqueData);
        printf("Fx: %f\t Fy: %f\t Fz: %f\t Tx: %f\t Ty: %f\t Tz: %f\n", ForceData[0], ForceData[1], ForceData[2], TorqueData[0], TorqueData[1], TorqueData[2]);

        if (my_counter%100==0)
		{
			//increment beckhoff analog out
            for (int i=0; i<4; ++i){
                bh_el4134.write_data_[i] += 1000;
                if (bh_el4134.write_data_[i]>32000){
                    bh_el4134.write_data_[i] = 0;
                }
            }

			// print force data
			printf("Fx: %f\t Fy: %f\t Fz: %f\t Tx: %f\t Ty: %f\t Tz: %f\n",
					ForceData[0], ForceData[1], ForceData[2],
					TorqueData[0], TorqueData[1], TorqueData[2]);

        }
        ++my_counter;

		//update the beckhoff stack master
        master.update();

        /* calculate next shot */
        t.tv_nsec += interval;

        while (t.tv_nsec >= NSEC_PER_SEC)
		{
			t.tv_nsec -= NSEC_PER_SEC;
            t.tv_sec++;
        }

        //usleep(500000);
    }


    return 0;
}
