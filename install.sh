#===============
# BUILD LIBRARY
#===============
SIMPLECAT_PATH="$(pwd)"

cd ${SIMPLECAT_PATH}
mkdir build
cd build
cmake ..
make -j3


#============================
# BUILD EXAMPLE APPLICATIONS
#============================
SIMPLECAT_EXAMPLE_PATH=${SIMPLECAT_PATH}/examples/

cd ${SIMPLECAT_EXAMPLE_PATH}/signal_loop
mkdir build
cd build
#cmake ..
#make -j3

cd ${SIMPLECAT_EXAMPLE_PATH}/nano_sleep
mkdir build
cd build
#cmake ..
#make -j3
