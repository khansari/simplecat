/**
 * \class Master
 *
 * \ingroup SimplECAT
 *
 * \brief Master for reading and writing to slave devices.
 *
 * This class allows mulitple EtherCAT slaves to be configured and run
 * using EtherLab's EtherCAT Master 1.5.2 code.
 */


#ifndef SIMPLECAT_MASTER_H_
#define SIMPLECAT_MASTER_H_

#include "ecrt.h"
#include <string>
#include <vector>
#include <map>

namespace simplecat {

class Slave;

class Master
{
public:
    Master(const int master = 0);
    virtual ~Master();

    /** \brief add a slave device to the master
      * alias and position can be found by running the following command
      * /opt/etherlab/bin$ sudo ./ethercat slaves
      * look for the "A B:C STATUS DEVICE" (e.g. B=alias, C=position)
      */
    void addSlave(uint16_t alias, uint16_t position, Slave* slave);

    /** call after adding all slaves, and before update */
    void activate();

    /** set the thread to a priority of -19
     *  priority range is -20 (highest) to 19 (lowest) */
    static void setThreadHighPriority();

    /** set the thread to real time (FIFO)
     *  thread cannot be preempted. even higher priority than -20 */
    static void setThreadRealTime();

    /** perform one EtherCAT cycle, passing the domain to the slaves */
    virtual void update(unsigned int domain = 0);

private:

    /** register a domain of the slave */
    struct DomainInfo;
    void registerPDOInDomain(uint16_t alias, uint16_t position,
                             unsigned int num_pdo_regs, DomainInfo* domain_info,
                             Slave* slave);

    /** check for change in the domain state */
    void checkDomainState(unsigned int domain);

    /** check for change in the master state */
    void checkMasterState();

    /** check for change in the slave states */
    void checkSlaveStates();

    static void printWarning(const std::string& message);

    /** EtherCAT master data */
    ec_master_t *master_ = NULL;
    ec_master_state_t master_state_ = {};

    /** data for a single domain */
    struct DomainInfo
    {
        DomainInfo(ec_master_t* master);
        ~DomainInfo();

        ec_domain_t *domain = NULL;
        ec_domain_state_t domain_state = {};
        uint8_t *domain_pd = NULL;

        /** domain pdo registration array.
         *  do not modify after active(), or may invalidate */
        std::vector<ec_pdo_entry_reg_t> domain_regs;

        /** slave's pdo entries in the domain */
        struct Entry {
            Slave* slave               = NULL;
            int num_pdos               = 0;
            unsigned int* offset       = NULL;
            unsigned int* bit_position = NULL;
        };

        std::vector<Entry> entries;
    };

    /** map from domain index to domain info */
    std::map<unsigned int, DomainInfo*> domain_info_;

    /** data needed to check slave state */
    struct SlaveInfo {
        Slave*                  slave               = NULL;
        ec_slave_config_t*      config              = NULL;
        ec_slave_config_state_t config_state        = {0};
    };

    std::vector<SlaveInfo> slave_info_;
};

}

#endif
